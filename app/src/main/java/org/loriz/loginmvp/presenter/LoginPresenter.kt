package org.loriz.loginmvp.presenter

import org.loriz.loginmvp.model.IBaseLoginInteractor
import org.loriz.loginmvp.model.LoginInteractor
import org.loriz.loginmvp.view.IBaseLoginView

class LoginPresenter(val view : IBaseLoginView) : IBaseLoginPresenter {

    private val interactor : IBaseLoginInteractor = LoginInteractor(this)

    override fun onLogin(username: String, pwd: String) {
        interactor.tryLogin(username, pwd)
    }

    override fun onLoginSuccess() {
        view.navigateToMainSection()
    }

    override fun onLoginFailure() {
        view.showError()
    }
}