package org.loriz.loginmvp.presenter

interface IBaseLoginPresenter {
    fun onLogin(username: String, pwd: String)
    fun onLoginSuccess()
    fun onLoginFailure()
}