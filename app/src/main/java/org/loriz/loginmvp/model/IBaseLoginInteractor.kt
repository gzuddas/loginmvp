package org.loriz.loginmvp.model

interface IBaseLoginInteractor {
    fun tryLogin(username : String, password : String)
}