package org.loriz.loginmvp.model

import org.loriz.loginmvp.presenter.IBaseLoginPresenter

class LoginInteractor(val presenter: IBaseLoginPresenter) : IBaseLoginInteractor {

    var dataManager: DataManager = DataManager()

    override fun tryLogin(username: String, password: String) {

        when (dataManager.recoverUserDB()[username]) {
            password -> presenter.onLoginSuccess()
            else -> presenter.onLoginFailure()
        }

    }

}