package org.loriz.loginmvp.model

class DataManager {

    var userDB : HashMap<String, String>? = null

    fun recoverUserDB() : HashMap<String, String> {

        return userDB ?: run {userDB = hashMapOf<String, String>("marco.loriga@gmail.com" to "testtest",
                "ilcazzoelamerda@yahoo.com" to "password123",
                "admin@admin.com" to "adminSafePassword"); userDB!!}
    }

}