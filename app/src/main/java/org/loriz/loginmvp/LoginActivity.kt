package org.loriz.loginmvp

import android.app.Activity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.section_login.*
import org.loriz.loginmvp.presenter.IBaseLoginPresenter
import org.loriz.loginmvp.presenter.LoginPresenter
import org.loriz.loginmvp.view.IBaseLoginView

class LoginActivity : Activity(), IBaseLoginView {

    private var mPresenter: IBaseLoginPresenter = LoginPresenter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.section_login)

        login_button.setOnClickListener {
            mPresenter.onLogin(login_username.text.toString(), login_pwd.text.toString())
        }

    }


    override fun navigateToMainSection() {
        Toast.makeText(this, "Ok! Navigate!", Toast.LENGTH_SHORT).show()
    }

    override fun showError() {
        Toast.makeText(this, "Invalid credentials!", Toast.LENGTH_SHORT).show()
    }

    override fun setPresenter(presenter: IBaseLoginPresenter) {
        this.mPresenter = presenter
    }

}