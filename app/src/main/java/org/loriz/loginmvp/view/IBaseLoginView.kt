package org.loriz.loginmvp.view

import org.loriz.loginmvp.presenter.IBaseLoginPresenter
import org.loriz.loginmvp.presenter.LoginPresenter

interface IBaseLoginView {
    fun setPresenter(presenter: IBaseLoginPresenter)
    fun navigateToMainSection()
    fun showError()
}