package org.loriz.loginmvp

import org.junit.Test

import org.junit.runner.RunWith
import org.mockito.Mockito.*
import org.loriz.loginmvp.presenter.LoginPresenter
import org.mockito.junit.MockitoJUnitRunner

/**
 * Tests
 */
@RunWith(MockitoJUnitRunner::class)
class LoginPresenterUnitTest {

    // mock views
    val view: LoginActivity = mock(LoginActivity::class.java)

    // presenter instance
    var presenter: LoginPresenter = LoginPresenter(view)

    //credentials
    val correctCredentialsMap: HashMap<String, String> = hashMapOf<String, String>("marco.loriga@gmail.com" to "testtest", "ilcazzoelamerda@yahoo.com" to "password123", "admin@admin.com" to "adminSafePassword")
    val incorrectCredentialsMap: HashMap<String, String> = hashMapOf("cacca" to "pupu", "lello" to "nenno", "luciano" to "signole")

    //tests
    @Test
    fun givenCorrectCredentials_whenTryToLogin_thenAssureSuccess() {

        correctCredentialsMap.forEach {
            presenter.onLogin(it.key, it.value)
        }
        verify(view, times(3)).navigateToMainSection()

    }

    @Test
    fun givenWrongCredentials_whenTryToLogin_thenAssureFailure() {

        incorrectCredentialsMap.forEach {
            presenter.onLogin(it.key, it.value)
        }
        verify(view, times(3)).showError()

    }
}
